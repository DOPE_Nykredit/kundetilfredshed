package com.nykredit.kundeservice.interneopgaver.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.nykredit.kundeservice.data.CTIRConnection;

public class SOracle extends CTIRConnection{
		
	public SOracle(String program){
		try {this.Connect();
		} catch (SQLException e) {e.printStackTrace();}
		this.SetupSpy(program);
		this.SS_spy();
	}
	
	public ResultSet getTotal(String whereClause,String startDate, String endDate){
	
		String str = 
				"SELECT 																									" +
				"	KS.DIGITPRESSED_NR2 AS KARAKTER, 																		" +
				"	COUNT(TRUNC(KS.TIDSPUNKT)) AS ANTAL_BESVARELSER,														" +
				" 	COUNT (CASE KS.DIGITPRESSED_NR2 WHEN '5' THEN 1 ELSE NULL END) AS ANTAL_BESVARELSER5						" +
				"FROM 																										" +
				"KS_DRIFT.KUNDETILFREDSHEDSMÅLING_SVAR KS 																	" +
				" "+WHERE(whereClause)+"																					" +
				"WHERE KS.DIGITPRESSED_NR2 IN ('1','2','3','4','5') AND 													" +
				"TRUNC(KS.TIDSPUNKT)>='"+startDate+"' AND TRUNC(KS.TIDSPUNKT)<='"+endDate+"' 								" +																				
				"GROUP BY KS.DIGITPRESSED_NR2 																				" + 
				"ORDER BY KS.DIGITPRESSED_NR2 																				" ;
			
		
		ResultSet rs = null;
		try {rs = this.Hent_tabel(str);} catch (SQLException e) {e.printStackTrace();}
		return rs;
	}
	
	private String WHERE(String whereClause) {
		return "INNER JOIN KS_DRIFT.V_TEAM_DATO VT							\n"+
		"ON KS.FIRST_AGENT = VT.INITIALER									\n"+
		"AND TRUNC(KS.TIDSPUNKT) = VT.DATO									\n"+	
		"INNER JOIN KS_DRIFT.AGENTER_TEAMS AT								\n"+
		"ON VT.TEAM = AT.TEAM " + whereClause;
	}

	
}

