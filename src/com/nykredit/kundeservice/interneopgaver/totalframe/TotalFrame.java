package com.nykredit.kundeservice.interneopgaver.totalframe;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;


import com.nykredit.kundeservice.interneopgaver.database.SOracle;
import com.nykredit.kundeservice.interneopgaver.tasksolveframe.TaskSolveFrame;
import com.nykredit.kundeservice.swing.DateSelector;
import com.nykredit.kundeservice.swing.KSCheckBoxTree;
import com.nykredit.kundeservice.swing.NComparator;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.swing.NTable;

import com.nykredit.kundeservice.util.Formatter;



public class TotalFrame extends NFrame implements WindowListener {
	
	private static final long serialVersionUID = 1L;
	private DateSelector dateSelector;
	private SOracle sOracle;
	private JTable table;
	private Formatter bf = new Formatter();
	private JScrollPane tableScrollPane;
	private KSCheckBoxTree KSCBT = null;
	protected String rs;
	
	
	public TotalFrame(SOracle sOracle,TaskSolveFrame parent){
		this.sOracle = sOracle;
		this.setTitle("Kundetilfredshed");
		this.setMinimumSize(new Dimension(420, 650));
		KSCBT = new KSCheckBoxTree(sOracle);
		this.setContentPane(getMainPane());
		WindowListener[] test = this.getWindowListeners();
		for (WindowListener list : test) {
			this.removeWindowListener(list);
		}
		this.addWindowListener(this);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
	}

	private DateSelector getDateSelector(){
		dateSelector = new DateSelector();
		return dateSelector;
	}
	
	private JPanel getMainPane(){	
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		p.add(getDateSelector(),c);
		c.insets = new Insets(10, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 1;
		p.add(getDataButton(),c);	
		c.insets = new Insets(10, 0, 0, 0);
		c.gridx = 0;
		c.gridy = 2;
		p.add(KSCBT,c);
		c.insets = new Insets(10, 10, 0, 0);
		c.gridx = 1;
		c.gridy = 2;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.gridwidth = 1;
		p.add(getTable(),c);	
		
		return p;
		
	}
	
	private JButton getDataButton(){
		JButton b = new JButton("Hent");
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				getTableData(KSCBT.getSelectionWhereClause("VT.","AT."), TotalFrame.this.dateSelector.getStart(), TotalFrame.this.dateSelector.getEnd());
			}
		});
		b.addKeyListener(new KeyListener() {
		public void keyPressed(KeyEvent e) {
		    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
		    	getTableData(KSCBT.getSelectionWhereClause("VT.","AT."), TotalFrame.this.dateSelector.getStart(), TotalFrame.this.dateSelector.getEnd());
		    	
	        }
		}
		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {}
		});
		return b;
	}

	private JScrollPane getTable(){
		this.KSCBT.getSelectionWhereClause("VT.","AT.");
		tableScrollPane = new JScrollPane();
		table = new NTable();
		tableScrollPane.setViewportView(table);
		return tableScrollPane;
	}
	
	private void getTableData(String whereClause,String startDate, String endDate){
	
		ResultSet rs = sOracle.getTotal(whereClause,startDate, endDate);
		
		DefaultTableModel m = new DefaultTableModel();
		table.setModel(m);
		m.addColumn("Karakter");
		m.addColumn("Antal Besvarelser");
		
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);

		TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(m);
		table.setRowSorter(sorter);
		sorter.setComparator(1, new NComparator());	

		table.setAutoResizeMode(0);
		table.setDefaultRenderer(Object.class, new TotalTableRenderer()); 
	
		double karakter = 0;
		double antal_besvarelser = 0;
		double antal_besvarelser5 = 0;
				
		try {
			while (rs.next()){
				m.addRow(new Object[]{rs.getInt("KARAKTER"),bf.KiloDotFill(rs.getDouble("ANTAL_BESVARELSER"),false)});
			
				karakter += rs.getInt("KARAKTER");
				antal_besvarelser += rs.getDouble("ANTAL_BESVARELSER");
				antal_besvarelser5 += rs.getDouble("ANTAL_BESVARELSER5");

			}
		} catch (SQLException e) {e.printStackTrace();}
		
		if(karakter>0){
			m.addRow(new Object[]{"Meget tilfreds",bf.toProcent(antal_besvarelser5/(antal_besvarelser),false)});
		}
		

		table.repaint();
		table.validate();
		
		tableScrollPane.repaint();
		tableScrollPane.validate();
		
		table.setPreferredScrollableViewportSize(table.getPreferredSize());
		TotalFrame.this.pack();
			
	}

	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {this.dispose();}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}

}
