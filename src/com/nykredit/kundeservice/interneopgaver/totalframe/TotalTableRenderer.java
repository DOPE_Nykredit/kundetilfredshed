package com.nykredit.kundeservice.interneopgaver.totalframe;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import com.nykredit.kundeservice.swing.NTableRenderer;

public class TotalTableRenderer extends NTableRenderer  {
	
	private static final long serialVersionUID = 1L;

    public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){    
    	
    	Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);


		if (column > 1){this.setHorizontalAlignment(SwingConstants.CENTER);
		}else {this.setHorizontalAlignment(SwingConstants.LEFT);}
     	

		if(table.getValueAt(row, 0).toString().contentEquals("Meget tilfredse")){
			cell.setBackground(new Color(0,0,128));
			cell.setForeground(new Color(255,255,255));
		}
        //test
    	addMarking(cell,isSelected,new Color(0,0,128),80);
    	
        return cell; 
        

    }  
    
}