package com.nykredit.kundeservice.interneopgaver.tasksolveframe;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.nykredit.kundeservice.interneopgaver.database.SOracle;
import com.nykredit.kundeservice.interneopgaver.totalframe.TotalFrame;
import com.nykredit.kundeservice.swing.NFrame;

public class TaskSolveFrame extends NFrame implements WindowListener {

	private static final long serialVersionUID = 1L;
	private SOracle sOracle;
	
	public TaskSolveFrame(String program){
		WindowListener[] test = this.getWindowListeners();
		for (WindowListener list : test) {
			this.removeWindowListener(list);
		}
		this.addWindowListener(this);
		sOracle = new SOracle(program);
		this.setContentPane(getMainPanel());
		this.setTitle(program+" - "+getAgent());
		this.pack();
		this.setLocationRelativeTo(getRootPane());
		this.setResizable(false);
		this.setVisible(true);

	}
	
	private JPanel getMainPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(5,5,0,5);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 2;		
		p.add(getButtonPanel(),c);
		
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.EAST;
		c.gridy = 3;
		c.insets = new Insets(5,5,5,5);
		p.add(new JLabel("CRPE � Nykredit"),c);	
		
		return p;
	}
	
	
	private JPanel getButtonPanel(){
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
			
		c.gridx = 0;
		c.gridy = 0;
		p.add(getTotalFrame(),c);
		
		return p;
	}
			
	private JButton getTotalFrame(){
		JButton b = new JButton("Kundetilfredshed");
		b.setPreferredSize(new Dimension(250,20));
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent e) {
				TotalFrame totalFrame = new TotalFrame(sOracle,TaskSolveFrame.this);
		    	totalFrame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
			}
		});
		b.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
			    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			    	TotalFrame totalFrame = new TotalFrame(sOracle,TaskSolveFrame.this);
			    	totalFrame.setDefaultCloseOperation( WindowConstants.DO_NOTHING_ON_CLOSE );
		        }
			}
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
			});
		return b;
	}
	
	public String getAgent(){
		return System.getenv("USERNAME").toUpperCase();
	}
	
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {this.dispose();}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	
}
